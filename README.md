# LipidHunter Instructions (Windows executable version)#

![windows_screenshot.png](https://bitbucket.org/repo/eGABj8/images/3461561587-windows_screenshot.png)

This repository contains the the Windows .exe executable version LipidHunter.

LipidHunter Windows executable files were generated from source code using [`py2exe`](http://py2exe.org/). 

LipidHunter source code repository can be found here
https://bitbucket.org/SysMedOs/lipidhunter

Here we will provide general information how to download Windows executable version.
 
LipidHunter Windows executable version is provided for `64bit` version of Windows 7, 8, 8.1 and 10 only.

LipidHunter requires minimum 2.0 GHz CPU and 4 GB RAM to run (8 GB or 16 GB of RAM are recommended for multiple processing)

Though we try our best to keep Windows executable distribution up to date, we have no guarantee that it corresponds to the latest version of LipidHunter source code. Please check out the original LipidHunter repository for latest updates.

If you have any problems while using LipidHunter, please report it here: [https://bitbucket.org/SysMedOs/lipidhunter/issues](https://bitbucket.org/SysMedOs/lipidhunter/issues)

** Please read the following instructions before you start to run LipidHunter. **

### Instructions ###

* [How to install Windows executable version of LipidHunter](#markdown-header-how-to-install-windows-executable-version-of-lipidhunter)
* [License](#markdown-header-license)
* [A step by step tutorial](https://bitbucket.org/SysMedOs/lipidhunter/wiki/Home)
* [Q&A](#markdown-header-further-questions)
* [Fundings](#markdown-header-fundings)



### How to install Windows executable version of LipidHunter ###
* Download the LipidHunter suitable for your system:

    + [LipidHunter for Windows 10 64bit ( `.zip` file around 310 MB)](https://bitbucket.org/SysMedOs/lipidhunter_exe/downloads/LipidHunter_Win10_64bit.zip)
    + [LipidHunter for Windows 7,8 and 8.1 64bit ( `.zip` file around 240 MB)](https://bitbucket.org/SysMedOs/lipidhunter_exe/downloads/LipidHunter_Win7-8_64bit.zip)
    
* Rename the downloaded file  to `LipidHunter.zip`
    
* Unzip `LipidHunter.zip` file to any folder. We recommend to use `7-zip` to unzip this file.

    + 7-Zip is open source software. [7-Zip home page](http://www.7-zip.org)
            
* **Optional: Resolve `.dll` issues. (Only do this step if LipidHunter do NOT run properly on your system)**

    LipidHunter is a free open source software. However, some system files might be required to execute the LipidHunter.exe. If you receive .dll error from LipidHunter or LipidHunter can not start, try to copy following system files to the LipidHunter folder and try again.

    + Copy following `.dll` and `.DRV` files from your system folder: `C:\WINDOWS\system32\` to the LipidHunter folder:

        + `.DRV` files:
        
                WINSPOOL.DRV - C:\WINDOWS\system32\WINSPOOL.DRV
            
        + `.dll` files:
    
                OLEAUT32.dll - C:\WINDOWS\system32\OLEAUT32.dll
                USER32.dll - C:\WINDOWS\system32\USER32.dll
                IMM32.dll - C:\WINDOWS\system32\IMM32.dll
                SHELL32.dll - C:\WINDOWS\system32\SHELL32.dll
                ole32.dll - C:\WINDOWS\system32\ole32.dll
                ODBC32.dll - C:\WINDOWS\system32\ODBC32.dll
                COMCTL32.dll - C:\WINDOWS\system32\COMCTL32.dll
                ADVAPI32.dll - C:\WINDOWS\system32\ADVAPI32.dll
                SHLWAPI.dll - C:\WINDOWS\system32\SHLWAPI.dll
                WS2_32.dll - C:\WINDOWS\system32\WS2_32.dll
                GDI32.dll - C:\WINDOWS\system32\GDI32.dll
                WINMM.dll - C:\WINDOWS\system32\WINMM.dll
                VERSION.dll - C:\WINDOWS\system32\VERSION.dll
                KERNEL32.dll - C:\WINDOWS\system32\KERNEL32.dll
                COMDLG32.dll - C:\WINDOWS\system32\COMDLG32.dll
                OPENGL32.dll - C:\WINDOWS\system32\OPENGL32.dll
            
        + For windows 7, 8, 8.1 additional `.dll` files might be required:
        
                msvcrt.dll - C:\WINDOWS\system32\msvcrt.dll
                ntdll.dll - C:\WINDOWS\system32\ntdll.dll

* Start LipidHunter by `LipidHunter.exe` 
    + The first time to start LipidHunter may take relative long time, please allow the software to run if LipidHunter got blocked by your anti-virus software.
    + We recommend you to create a shortcut of `LipidHunter.exe` on your desktop. The Windows 10 version shortcut comes with built in LipidHunter icon, and users of Windows 7-8 versions can find the `LipidHunter.ico` in LipidHunter folder to customize the shortcut.  

* Read LipidHunter user guide: [Download LipidHunter user guide PDF](https://bitbucket.org/SysMedOs/lipidhunter_exe/downloads/LipidHunter_User_Guide.pdf)
  
* Run the test files
    
    [LipidHunter_Test_mzML_File](https://bitbucket.org/SysMedOs/lipidhunter_exe/downloads/LipidHunter_TestFile.zip)

* Run your data

* Errors/bugs
    
    In case you experienced any problems with running LipidHunter, please report an issue in the [issue tracker](https://bitbucket.org/SysMedOs/lipidhunter/issues) or contact us.

### License ###

+ LipidHunter is Dual-licensed
    * For academic and non-commercial use: `GPLv2 License`: 
    
        [The GNU General Public License version 2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html)

    * For commercial use: please contact the develop team by email.


+ Please cite our publication in an appropriate form.

     - [Ni, Zhixu, Georgia Angelidou, Mike Lange, Ralf Hoffmann, and Maria Fedorova. "LipidHunter identifies phospholipids by high-throughput processing of LC-MS and shotgun lipidomics datasets." Analytical Chemistry (2017).](http://pubs.acs.org/doi/10.1021/acs.analchem.7b01126)
    
        * DOI: [`10.1021/acs.analchem.7b01126`](http://pubs.acs.org/doi/10.1021/acs.analchem.7b01126)

### Further questions? ###

* Read our [wiki](https://bitbucket.org/SysMedOs/lipidhunter/wiki/Home)
* Report any issues here: [https://bitbucket.org/SysMedOs/lipidhunter/issues](https://bitbucket.org/SysMedOs/lipidhunter/issues)


### Fundings ###
We acknowledge all projects that supports the development of LipidHunter:

+ BMBF - Federal Ministry of Education and Research Germany:

    https://www.bmbf.de/en/

+ e:Med Systems Medicine Network:

    http://www.sys-med.de/en/

+ SysMedOS Project : 

    https://home.uni-leipzig.de/fedorova/sysmedos/